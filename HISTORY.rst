=======
History
=======

0.1.1 (2019-12-06)
------------------
* Add basic documentation
* Bump nash-api requirement to 1.0.6 to allow LINK trading
* Remove some dead code

0.1.0 (2019-12-04)
------------------
* Public release
